package com.jd.lme.example.coffee.maker;

import com.jd.lme.example.config.SpringConfig;
import com.jd.lme.example.mock.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {SpringConfig.class})
public class MockBeanDemoTest {

    @MockBean
    UserRepository mockRepository;

    @Autowired
    ApplicationContext applicationContext;

    @Test
    public void givenCountMethodMocked_WhenCountInvoked_ThenMockValueReturned() {
        //指定mock对象的行为
        Mockito.when(mockRepository.count()).thenReturn(123);

        //通过应用程序上下文获取Mock的对象
        //也可以直接调用mockRepository.count()方法
        UserRepository userRepoFromContext = applicationContext.getBean(UserRepository.class);
        long userCount = userRepoFromContext.count();

        Assert.assertEquals(123, userCount);
        //验证被调用的就是Mock的对象
        Mockito.verify(mockRepository).count();
    }

}
