package com.jd.lme.example.pecs;

import java.time.Instant;
import java.time.temporal.TemporalField;
import java.util.concurrent.CountDownLatch;

public class PECSDemo {
    public static volatile int race = 0;

    public static void increase() {
        race++;
    }

    private static final int THREAD_COUNT = 20;

    public static void main(String[] args) throws InterruptedException {
        Thread[] threads = new Thread[THREAD_COUNT];
        CountDownLatch countDownLatch = new CountDownLatch(THREAD_COUNT);

        for (int i = 0; i < THREAD_COUNT; i++){
            threads[i] = new Thread(() -> {
                countDownLatch.countDown();
                for (int i1 = 0; i1 < 10000; i1++) {
                    increase();
                }
            });
            threads[i].start();
        }

        countDownLatch.await();

        System.out.println(race);
    }
}
