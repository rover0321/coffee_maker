package com.jd.lme.example;

import com.jd.lme.example.lazyInitDemo.NormalInitBean;
import com.yomahub.liteflow.core.FlowExecutor;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class LiteFlowDemoApplication {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("classpath:spring-beans.xml");
        FlowExecutor flowExecutor = classPathXmlApplicationContext.getBean(FlowExecutor.class);
        flowExecutor.execute2Resp("chain1");

        NormalInitBean normalInitBean = classPathXmlApplicationContext.getBean(NormalInitBean.class);
        normalInitBean.sayHello();

    }
}
