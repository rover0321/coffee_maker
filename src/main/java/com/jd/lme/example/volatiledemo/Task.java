package com.jd.lme.example.volatiledemo;

import java.util.concurrent.CountDownLatch;

public class Task implements Runnable{
    private volatile int count = 0;
    private final CountDownLatch countDownLatch;

    public Task(CountDownLatch countDownLatch) {
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void run() {
        this.count++;
        System.out.println("xxxxx" + count + "xxxxx");
        this.countDownLatch.countDown();
    }

    public int getCount() {
        return count;
    }
}
