package com.jd.lme.example.volatiledemo;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class VolatileDemo {

    private static ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(100, 100,
            100, TimeUnit.SECONDS, new ArrayBlockingQueue<>(1024));

    public static void main(String[] args) throws InterruptedException {
        final int MAX_TASK = 1000;
        CountDownLatch countDownLatch = new CountDownLatch(MAX_TASK);
        Task task = new Task(countDownLatch);
        for (int i = 0; i < MAX_TASK; i++) {
            threadPoolExecutor.submit(task);
        }

        countDownLatch.await();
        System.out.println("=====" + task.getCount());
    }

}
