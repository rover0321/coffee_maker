package com.jd.lme.example.coffee.maker.favors;

public class FavorFactory {
    public static Favor createFavor(FavorEnum favorEnum){
        Favor favor;
        switch (favorEnum){
            case Low:
                favor = new LowSugar(); break;
            case Half:
                favor = new HalfSugar(); break;
            case High:
                favor = new HighSugar(); break;
            case Full:
                favor = new FullSugar(); break;
            default:
                favor = new NoSugar(); break;
        }
        return favor;
    }

}
