package com.jd.lme.example.coffee.maker;

import com.jd.lme.example.coffee.maker.builders.Builders;
import com.jd.lme.example.coffee.maker.builders.CoffeeBuilder;
import com.jd.lme.example.coffee.maker.favors.Favor;
import com.jd.lme.example.coffee.maker.favors.FavorEnum;
import com.jd.lme.example.coffee.maker.favors.FavorFactory;

public class CoffeeMaker {

    Coffee makeCoffee(CoffeeType coffeeType, FavorEnum favorEnum){
        Favor favor = FavorFactory.createFavor(favorEnum);
        CoffeeBuilder coffeeBuilder = Builders.newBuilder(coffeeType);
        return coffeeBuilder.favor(favor).build();
    }
}
