package com.jd.lme.example.coffee.maker.favors;

public enum FavorEnum {
    No,
    Low,
    Half,
    High,
    Full
}
