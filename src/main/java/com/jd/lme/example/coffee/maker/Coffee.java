package com.jd.lme.example.coffee.maker;

import com.jd.lme.example.coffee.maker.favors.Favor;
import com.jd.lme.example.coffee.maker.materials.Material;

import java.util.HashMap;
import java.util.Map;

public abstract class Coffee implements Material {
    private Map<Material, Integer> composition = new HashMap<>();
    private Favor favor;


    public void add(Material material, Integer ratio) {
        this.composition.put(material, ratio);
    }

    public void addFavor(Favor favor) {
        this.favor = favor;
    }

    public String desc() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(favor.desc()).append(name()).append("\n");
        composition.forEach((material, integer) -> {
            String oneLine = material.desc() + " : " + integer + "份";
            stringBuilder.append(oneLine).append("\n");
        });
        return stringBuilder.toString();
    }

    public abstract String name();

}
