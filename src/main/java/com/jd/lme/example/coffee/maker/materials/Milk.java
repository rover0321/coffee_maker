package com.jd.lme.example.coffee.maker.materials;

/**
 * 热牛奶
 */
public class Milk implements Material {

    @Override
    public String desc() {
        return "牛奶";
    }
}
