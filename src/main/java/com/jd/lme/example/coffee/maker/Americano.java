package com.jd.lme.example.coffee.maker;



/**
 * 美式咖啡
 */
public class Americano extends Coffee {

    public Americano() {
    }

    @Override
    public String name() {
        return "美式咖啡";
    }

}
