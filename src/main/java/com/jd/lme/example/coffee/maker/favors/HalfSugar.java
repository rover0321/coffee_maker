package com.jd.lme.example.coffee.maker.favors;

public class HalfSugar implements Favor{
    @Override
    public String desc() {
        return "半糖";
    }
}
