package com.jd.lme.example.coffee.maker.favors;

public interface Favor {
    String desc();
}
