package com.jd.lme.example.coffee.maker.favors;

/**
 * 高糖
 */
public class HighSugar implements Favor{
    @Override
    public String desc() {
        return "高糖";
    }
}
