package com.jd.lme.example.coffee.maker;

import com.jd.lme.example.coffee.maker.favors.FavorEnum;

/**
 * Hello world!
 *
 */
public class Client
{
    public static void main( String[] args )
    {
        CoffeeMaker coffeeMaker = new CoffeeMaker();
        Coffee coffee = coffeeMaker.makeCoffee(CoffeeType.Cappuccino, FavorEnum.Half);
        String desc = coffee.desc();
        System.out.println(desc);


        String pList = "p3,";
        String[] split = pList.split(",");
        System.out.println(split);
    }
}
