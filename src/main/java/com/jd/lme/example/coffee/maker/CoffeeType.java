package com.jd.lme.example.coffee.maker;

public enum CoffeeType {
    Americano,
    Cappuccino,
    Latte,
    Mocha
}
