package com.jd.lme.example.coffee.maker.builders;


import com.jd.lme.example.coffee.maker.Coffee;
import com.jd.lme.example.coffee.maker.favors.Favor;
import com.jd.lme.example.coffee.maker.materials.Material;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractCoffeeBuilder implements CoffeeBuilder {
    protected Map<Material, Integer> materialIntegerMap = new HashMap<>();
    protected Favor favor;
    protected Coffee coffee;

    protected void grindCoffeeBean() {
        System.out.println("步骤一：磨咖啡豆");
    }


    protected void addFavor(Coffee coffee) {
        System.out.println("步骤三：添加口味");
        coffee.addFavor(favor);
    }

    protected abstract void addMaterial();

    @Override
    public CoffeeBuilder favor(Favor favor) {
        this.favor = favor;
        return this;
    }

    @Override
    public Coffee build() {
        grindCoffeeBean(); // 第一步
        addMaterial(); // 第二步
        addFavor(this.coffee);
        return coffee;
    }
}
