package com.jd.lme.example.coffee.maker.materials;

/**
 * 原材料抽象类
 */
public interface Material {
    String desc();
}
