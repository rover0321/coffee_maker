package com.jd.lme.example.coffee.maker.materials;

/**
 * 奶泡
 */
public class MilkFoam implements Material{
    @Override
    public String desc() {
        return "奶泡";
    }
}
