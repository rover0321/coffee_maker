package com.jd.lme.example.coffee.maker.favors;

/**
 * 低糖
 */
public class LowSugar implements Favor{
    @Override
    public String desc() {
        return "低糖";
    }
}
