package com.jd.lme.example.coffee.maker.materials;

/**
 * 水
 */
public class Water implements Material {

    @Override
    public String desc() {
        return "水";
    }
}
