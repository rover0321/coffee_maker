package com.jd.lme.example.coffee.maker.builders;

import com.jd.lme.example.coffee.maker.materials.CoffeeBean;
import com.jd.lme.example.coffee.maker.materials.Milk;
import com.jd.lme.example.coffee.maker.materials.MilkFoam;
import com.jd.lme.example.coffee.maker.Cappuccino;

public class CappuccinoCoffeeBuilder extends AbstractCoffeeBuilder{
    public CappuccinoCoffeeBuilder() {
        this.coffee = new Cappuccino();
    }

    @Override
    protected void addMaterial() {
        System.out.println("步骤二：添加原料");
        Milk milk = new Milk();
        CoffeeBean coffeeBean = new CoffeeBean();
        MilkFoam milkFoam = new MilkFoam();

        coffee.add(coffeeBean, 1);
        coffee.add(milk, 1);
        coffee.add(milkFoam, 1);
    }
}
