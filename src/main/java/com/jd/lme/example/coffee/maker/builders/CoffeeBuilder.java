package com.jd.lme.example.coffee.maker.builders;

import com.jd.lme.example.coffee.maker.Coffee;
import com.jd.lme.example.coffee.maker.favors.Favor;

public interface CoffeeBuilder {

    CoffeeBuilder favor(Favor favor);

    Coffee build();
}
