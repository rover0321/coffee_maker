package com.jd.lme.example.lazyInitDemo;

public class NormalInitBean {
    private LazyInitBean lazyInitBean;

    public LazyInitBean getLazyInitBean() {
        return lazyInitBean;
    }

    public void setLazyInitBean(LazyInitBean lazyInitBean) {
        this.lazyInitBean = lazyInitBean;
    }

    public void sayHello() {
        System.out.println("hello");
    }
}
