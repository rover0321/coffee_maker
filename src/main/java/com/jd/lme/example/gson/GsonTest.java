package com.jd.lme.example.gson;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.List;

public class GsonTest {
    public static void main(String[] args) throws FileNotFoundException {
        String filename = "/Users/huangxiang46/wks/coffee_maker/辩证词条.json";
        Type type = new TypeToken<List<Organic>>() {
        }.getType();
        Gson gson = new Gson();
        JsonReader reader = new JsonReader(new FileReader(filename));
        List<Organic> data = gson.fromJson(reader, type);
        System.out.println(gson.toJson(data));
    }
}
