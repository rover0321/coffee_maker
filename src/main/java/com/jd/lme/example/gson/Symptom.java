package com.jd.lme.example.gson;

import lombok.Data;

/**
 * 症状 an effect of an illness
 */
@Data
public class Symptom {
    private String name;
}
