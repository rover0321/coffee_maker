package com.jd.lme.example.gson;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.List;

@Data
public class Organic {
    private String name;
    @SerializedName(value = "syndromes", alternate = "outline")
    private List<Syndrome> syndromes;
}
