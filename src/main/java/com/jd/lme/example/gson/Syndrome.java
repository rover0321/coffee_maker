package com.jd.lme.example.gson;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.List;

/**
 * a group of symptoms that all happen at the same time that indicate a specific cause
 */
@Data
public class Syndrome {
    private String name;

    @SerializedName(value = "symptoms", alternate = "outline")
    private List<Symptom> symptoms;
}
