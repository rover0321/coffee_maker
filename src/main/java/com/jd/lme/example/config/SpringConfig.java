package com.jd.lme.example.config;


import com.jd.lme.example.mock.UserRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringConfig {

    @Bean
    UserRepository userRepository() {
        return new UserRepository();
    }
}
