package com.jd.lme.example.patterns.interpretpattern;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class OrExpression implements Expression{
    private final List<Expression> expressions = new ArrayList<>();

    public OrExpression(String strOrExpression) {
        String[] andStrExpressions = strOrExpression.split("\\|\\|");
        for (String andStrExpression : andStrExpressions) {
            expressions.add(new AndExpression(andStrExpression));
        }
    }

    @Override
    public boolean interpret(Map<String, Long> stats) {
        for (Expression expression : expressions) {
            if (expression.interpret(stats)) {
                return true;
            }
        }
        return false;
    }
}
