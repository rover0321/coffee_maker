package com.jd.lme.example.patterns.statepattern;

public abstract class BaseMarioStateMachine implements MarioStateMachine{
    protected int score;
    protected State currentState;

    protected BaseMarioStateMachine() {
        this.score = 0;
        this.currentState = State.SMALL;
    }

    public int getScore() {
        return score;
    }

    public State getCurrentState() {
        return currentState;
    }
}
