package com.jd.lme.example.patterns.visitor;

import java.util.Random;

public class Engineer extends Employee{
    public Engineer(String name) {
        super(name);
    }

    @Override
    public void accept(IVisitor visitor) {
        visitor.visit(this);
    }
    //工程师一年的代码量
    public int getCodeLines() {
        return new Random().nextInt(10 * 10000);
    }
}
