package com.jd.lme.example.patterns.pipeline;

public class CharacterReverseHandler implements Handler<String, String>{
    @Override
    public String process(String input) {
        System.out.println("===反转字符串===");
        return new StringBuilder(input).reverse().toString();
    }
}
