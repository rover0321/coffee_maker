package com.jd.lme.example.patterns.statepattern;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MarioGameApplication {
    public static void main(String[] args) throws InterruptedException {
        MarioStateMachine marioStateMachine = new DivisionMarioStateMachine();
        marioStateMachine.obtainMushroom();

        log.info("currentState={}, currentScore={}", marioStateMachine.getCurrentState(), marioStateMachine.getScore());

        Thread.sleep(2000);
    }
}
