package com.jd.lme.example.patterns.pipeline;

import lombok.val;

import java.io.File;

public class ClientTest {
    public static void main(String[] args) {
        File file = new File("src/main/java/com/jd/lme/example/patterns/pipeline/hello.txt");
        val filters = new Pipeline<>(new FileProcessHandler())
                .addHandler(new CharacterFilterHandler())
                .addHandler(new CharacterReverseHandler());
        System.out.println(filters.execute(file));
    }
}
