package com.jd.lme.example.patterns.interpretpattern;

import java.util.HashMap;
import java.util.Map;

public class AlertRuleInterpreter {
    private final Expression expression;

    public AlertRuleInterpreter(String ruleExpression) {
        this.expression = new OrExpression(ruleExpression);
    }

    public boolean interpret(Map<String, Long> stats) {
        return expression.interpret(stats);
    }


    public static void main(String[] args) {
        String rule = "key1 > 100 && key2 < 30 && key3 < 100 && key4 == 88 || key1 < 20";

        AlertRuleInterpreter alertRuleInterpreter = new AlertRuleInterpreter(rule);

        Map<String, Long> stats = new HashMap<>();
        stats.put("key1", 17L);
        stats.put("key2", 10L);
        stats.put("key3", 12L);
        stats.put("key4", 88L);

        boolean alert = alertRuleInterpreter.interpret(stats);
        System.out.println(alert);

    }
}
