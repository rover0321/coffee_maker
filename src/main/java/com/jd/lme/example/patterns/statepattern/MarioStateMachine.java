package com.jd.lme.example.patterns.statepattern;

public interface MarioStateMachine {

    void obtainMushroom();

    void obtainCape();

    void obtainFireFlower();

    void meetMonster();

    int getScore();

    State getCurrentState();
}
