package com.jd.lme.example.patterns.pipeline;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * 一个具体的文件处理器
 * @author huangxiang46
 */
public class FileProcessHandler implements Handler<File, List<String>>{
    @Override
    public List<String> process(File file) {
        System.out.println("===文件处理===");
        try {
            return FileUtils.readLines(file, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }
}
