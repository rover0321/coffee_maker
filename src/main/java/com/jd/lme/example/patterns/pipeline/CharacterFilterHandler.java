package com.jd.lme.example.patterns.pipeline;

import java.util.List;
import java.util.stream.Collectors;

public class CharacterFilterHandler implements Handler<List<String>, String>{
    @Override
    public String process(List<String> lines) {
        System.out.println("===字符过滤===");
        List<String> filteredLines = lines.stream().filter(s -> s.contains("hello")).collect(Collectors.toList());
        return String.join("", filteredLines);
    }
}
