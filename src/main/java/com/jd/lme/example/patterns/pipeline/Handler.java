package com.jd.lme.example.patterns.pipeline;

/**
 * 处理器接口定义
 * @author huangxiang46
 */
public interface Handler<I, O> {
    /**
     * 处理器需要实线的接口
     * @param input 当前阶段需要处理的输入
     * @return 当前阶段处理之后的输出
     */
    O process(I input);
}
