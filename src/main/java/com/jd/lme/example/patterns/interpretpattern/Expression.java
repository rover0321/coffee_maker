package com.jd.lme.example.patterns.interpretpattern;

import java.util.Map;

public interface Expression {
    boolean interpret(Map<String, Long> stats);
}
