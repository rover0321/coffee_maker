package com.jd.lme.example.patterns.statepattern;

public enum State {
    SMALL(0),
    SUPER(1),
    CAPE(2),
    FIRE(3);
    private final int value;

    State(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }


    @Override
    public String toString() {
        if (this.value == 0) {
            return "small";
        } else if (this.value == 1) {
            return "super";
        } else if (this.value == 2) {
            return "cope";
        } else {
            return "fire";
        }
    }
}
